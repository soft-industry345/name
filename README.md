# API testing ( https://reqres.in/ )

## Required software to run

- Java 8
- Maven

## Start instructions

1) Run command: **mvn clean verify**
2) Follow the path to view the report: **/target -> site -> serenity -> index.html**
