@DeleteUserFeature @Users @API @Regression
Feature: Delete User

  @Positive
  Scenario: Delete user success
    Given [api] set id to '1'
    When  [api] send delete user request
    Then  [api] delete user status code must be '204'

  @Negative
  Scenario: Delete user with non-existent id
    Given [api] set id to '7423464'
    When  [api] send delete user request
    Then  [api] delete user status code must be '204'
    #    to do when bug will be fixer uncomment next line
#    Then  [api] create user status code must be '400'

  @Negative
  Scenario: Delete user with 0 id
    When  [api] send delete user request
    Then  [api] delete user status code must be '204'
    #    to do when bug will be fixer uncomment next line
#    Then  [api] create user status code must be '400'