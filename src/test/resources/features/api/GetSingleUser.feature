@GetSingleUserFeature @Users @API @Regression
Feature: Get Single User

  @Positive
  Scenario: Get single user success
    Given [api] set id to '2'
    When  [api] send get single user request
    Then  [api] get single user status code must be '200'
    And   [api] get single user response equals with request

  @Negative
  Scenario: Get single user with without id
    When  [api] send get single user request
    Then  [api] get single user status code must be '404'

  @Negative
  Scenario: Get single user with non-existent id
    Given [api] set id to '424251412'
    When  [api] send get single user request
    Then  [api] get single user status code must be '400'
